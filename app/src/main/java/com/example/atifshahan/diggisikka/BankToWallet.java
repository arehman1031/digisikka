package com.example.atifshahan.diggisikka;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class BankToWallet extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_to_wallet);
    }
    public void BankTo_WalletAmount_Successful(View view) {
        Intent intent = new Intent(BankToWallet.this, MainFunctionalitiesPage.class);
        startActivity(intent);
    }

}

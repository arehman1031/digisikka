package com.example.atifshahan.diggisikka;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class LoginPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);
    }

    public void login(View view){
        Intent intent = new Intent(LoginPage.this,MainFunctionalitiesPage.class);
        startActivity(intent);
    }

    public void viewSignup(View view){

        Intent intent = new Intent(LoginPage.this,SignUpPage.class);
        startActivity(intent);
    }

}

package com.example.atifshahan.diggisikka;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainFunctionalitiesPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_functionalities_page);
    }


    public void NewAccount(View view){
        Intent intent = new Intent(MainFunctionalitiesPage.this,NewAccountPage.class);
        startActivity(intent);
    }

    public void Wallet (View view) {
        Intent intent = new Intent(MainFunctionalitiesPage.this, WalletPage.class);
        startActivity(intent);
    }

    public void Services (View view){
        Intent intent = new Intent(MainFunctionalitiesPage.this,MainServices.class);
        startActivity(intent);
    }

    public void Help (View view) {
        Intent intent = new Intent(MainFunctionalitiesPage.this, Help_Page.class);
        startActivity(intent);
    }
}

package com.example.atifshahan.diggisikka;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainServices extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_services);
    }

    public void BankToWallet(View view){
        Intent intent = new Intent(MainServices.this,BankToWallet.class);
        startActivity(intent);
    }

    public void WalletToWallet (View view) {
        Intent intent = new Intent(MainServices.this, BankToWallet.class);
        startActivity(intent);
    }

    public void WalletToBank (View view){
        Intent intent = new Intent(MainServices.this,WalletTobank.class);
        startActivity(intent);
    }

}

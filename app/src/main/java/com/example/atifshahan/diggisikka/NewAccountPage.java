package com.example.atifshahan.diggisikka;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class NewAccountPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_account_page);
    }

    public void NewAccountSuccessfull(View view) {
        Intent intent = new Intent(NewAccountPage.this, MainFunctionalitiesPage.class);
        startActivity(intent);
    }
}

package com.example.atifshahan.diggisikka;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class SignUpPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_page);
    }


    public void viewMainFunctionalities(View view){
        Intent intent = new Intent(SignUpPage.this,MainFunctionalitiesPage.class);
        startActivity(intent);

    }
}

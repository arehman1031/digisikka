package com.example.atifshahan.diggisikka;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class WalletToWallet_AmountTransfer extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_to_wallet__amount_transfer);
    }


    public void WalletTo_WalletAmount_Successful(View view) {
        Intent intent = new Intent(WalletToWallet_AmountTransfer.this, MainFunctionalitiesPage.class);
        startActivity(intent);
    }
}

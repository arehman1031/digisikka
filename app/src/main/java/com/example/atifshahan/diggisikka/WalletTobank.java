package com.example.atifshahan.diggisikka;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class WalletTobank extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_tobank);
    }

    public void WalletTo_bankAmount_Successful(View view) {
        Intent intent = new Intent(WalletTobank.this, MainFunctionalitiesPage.class);
        startActivity(intent);
    }
}